package com.demo.service.myapplication.bindService;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.demo.service.myapplication.R;

public class BoundServiceActivity extends AppCompatActivity implements View.OnClickListener{
    private BoundService myService;
    private boolean isBound = false;
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BoundService.MyBinder binder = (BoundService.MyBinder) service;
            myService = binder.getService(); // lấy đối tượng MyService
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bound_service);
        findViewById(R.id.btOff).setOnClickListener(this);
        findViewById(R.id.btTua).setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("TAG","onResume");
        Intent intent = new Intent(this, BoundService.class);
        bindService(intent,connection,BIND_AUTO_CREATE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btOff:
                // Nếu Service đang hoạt động
                if(isBound){
                    // Tắt Service
                    unbindService(connection);
                    isBound = false;
                }
                break;
            case R.id.btTua:
                // nếu service đang hoạt động
                if(isBound){
                    // tua bài hát
                    myService.fastForward();
                }else{
                    Toast.makeText(this, "Service chưa hoạt động", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
