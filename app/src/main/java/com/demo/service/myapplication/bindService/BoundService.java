package com.demo.service.myapplication.bindService;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Android Studio on 3/10/2018.
 */

public class BoundService extends Service {
    private MyPlayer myPlayer;
    private IBinder binder;

    @Override
    public void onCreate() {
        myPlayer = new MyPlayer(this);
        binder = new MyBinder();
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("TAG", "Đã gọi onBind()");
        myPlayer.play();
        // trả về đối tượng binder cho ActivityMain
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("TAG", "Đã gọi onBind()");
        myPlayer.stop();
        return super.onUnbind(intent);
    }

    /*
    * Phương thức dùng để tua bài hát
    *
    * */
    public void fastForward(){
        myPlayer.fastForward(60000); // tua đến giây thứ 60
    }

    public class MyBinder extends Binder {
        /**
         * Phương thức này dùng để trả về đối tượng BoundService
         * */
        public BoundService getService() {
            return BoundService.this;
        }
    }
}
