package com.demo.service.myapplication.localBM;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.ContextThemeWrapper;

/**
 * Created by Android Studio on 3/10/2018.
 */

public class LocalBMService extends Service{
    private Context mContext = this;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sendMessageToActivity("hello");
        return START_STICKY;
    }

    private void sendMessageToActivity(String msg) {
        Intent intent = new Intent("Tinnhan");
        // You can also include some extra data.
        intent.putExtra("mes", msg);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

}
