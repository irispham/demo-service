package com.demo.service.myapplication.bindService;

import android.content.Context;
import android.media.MediaPlayer;

import com.demo.service.myapplication.R;

/**
 * Created by Android Studio on 3/10/2018.
 */

public class MyPlayer {
    private MediaPlayer mediaPlayer;

    public MyPlayer(Context context) {
        // Nạp bài nhạc vào mediaPlayer
        mediaPlayer = MediaPlayer.create(context, R.raw.het_roi_ho_quang_hieu);
        // Đặt chế độ phát lặp lại liên tục
        mediaPlayer.setLooping(true);
    }

    public void fastForward(int pos){
        mediaPlayer.seekTo(pos);
    }
    // phát nhạc
    public void play() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }
    }

    // dừng phát nhạc
    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }
}
